The CiviCRM Group gmap module allows you to create pages that display Google maps of your CiviCRM Groups. It requires the Gmap module and CiviCRM. 

A CiviCRM Group gmap is a node that displays a google map based on information derived from a CiviCRM group. It maintains its own location data that is initialized from the CiviCRM data when the node is created.

For more information about CiviCRM see: http://civicrm.org/
